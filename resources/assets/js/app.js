global.$ = global.jQuery = require('jquery');

require('bootstrap');
require('./plugins/slowScroll');
require('./plugins/navigationScroll');
/*
 |--------------------------------------------------------------------------
 | Document Ready Function
 |--------------------------------------------------------------------------
 */
$(function() {

    'use strict';

    $('body').scrollspy({ target: '.navbar' });
});

$(function() {
  if (screen.width > 1800) {
    $('.container-changue').removeClass('container');
    $('.container-changue').addClass('container-fluid');
  }
});
$(window).on('scroll', function() {
// as you scroll this will continuously be fire:
// if the distance you've scrolled is greater than or equal to the top position of section 2 add the in-view class 
  if($(window).scrollTop() >= $('#section2-trigger').offset().top) {
    $('.sectionTwo').addClass('section-imageAnimation');
  }
  // as you scroll this will continuously be fire:
  // if the distance you've scrolled is greater than or equal to the top position of section 2 add the in-view class 
  if($(window).scrollTop() >= $('#section3-trigger').offset().top) {
    $('.sectionThree').addClass('section3-animacion');
  }
  // as you scroll this will continuously be fire:
  // if the distance you've scrolled is greater than or equal to the top position of section 2 add the in-view class 
  if($(window).scrollTop() >= $('#section4-trigger').offset().top) {
    $('.sectionFour').addClass('section4-animacion');
  }
  // as you scroll this will continuously be fire:
  // if the distance you've scrolled is greater than or equal to the top position of section 2 add the in-view class 
  if($(window).scrollTop() >= $('#section5-trigger').offset().top) {
    $('.sectionFive').addClass('section-imageAnimation');
  }
  // as you scroll this will continuously be fire:
  // if the distance you've scrolled is greater than or equal to the top position of section 2 add the in-view class 
  if($(window).scrollTop() >= $('#section6-trigger').offset().top) {
    $('.sectionSix').addClass('section5-animacion');
  } 
});